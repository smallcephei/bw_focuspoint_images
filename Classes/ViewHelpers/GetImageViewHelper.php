<?php

namespace Blueways\BwFocuspointImages\ViewHelpers;

use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;

class GetImageViewHelper extends AbstractTagBasedViewHelper
{
    /**
     *
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('image', 'mixed', 'image', true, '');

    }

    /**
     *
     */
    public function render() : object
    {
        $image = $this->arguments['image'];
        $storage = $image->storage;
        $identifier = $image->identifier;

        $resourceFactory = GeneralUtility::makeInstance(ResourceFactory::class);
        return $resourceFactory->getFileObjectFromCombinedIdentifier("$storage:$identifier");
    }
}