;(function($){
    var focuspoint = {

        init: function () {
            focuspoint.listenForFocuspointMouseEnter();
            focuspoint.listenForFocuspointMouseLeave();
            focuspoint.isMobileScreenSize = $(window).width() < 600;
            if (focuspoint.isMobileScreenSize) {
                focuspoint.preventMobileTouchLinks();
            }
        },

        listenForFocuspointMouseEnter: function () {
            var popoverIndex;
            $(".focuspoint .point").mouseenter(function () {
                popoverIndex = $(this).data("index");
                focuspoint.showPopover(popoverIndex, this);
            });
        },

        listenForFocuspointMouseLeave: function () {
            var popoverIndex;
            $(".focuspoint .popover").mouseleave(function () {
                popoverIndex = $(this).data("index");
                focuspoint.hidePopover(popoverIndex);
            });
        },

        showPopover: function(popoverIndex, point) {
            var position = $(point).position();
            var popover = $(".focuspoint .popover-"+popoverIndex);
            var popoverWidth = popover.width();
            var popoverHeight = popover.height();
            var posLeft = position.left - (popoverWidth/2);
            var posTop =  position.top - (popoverHeight/2);

            if (focuspoint.isMobileScreenSize) {
                // handle overflow of popover
                // necessary for small screens when popover is positioned out of screen
                var outOfDocument = focuspoint.isOutOfDocument(point, posLeft, posTop, popoverWidth, popoverHeight);
                if (outOfDocument.top) {
                    posTop = 0;
                }
                if (outOfDocument.right) {
                    posLeft = $(".focuspoint").width() - popover.width();
                }
                if (outOfDocument.bottom) {
                    posTop = $(".focuspoint").height() - popover.height();
                }
                if (outOfDocument.left) {
                    posLeft = 0;
                }
            }

            popover
                .css({
                    left: posLeft+"px",
                    top: posTop+"px"
                })
                .removeClass("hidden");
        },

        hidePopover: function(popoverIndex) {
            $(".focuspoint .popover-"+popoverIndex).addClass("hidden");
            if (focuspoint.isMobileScreenSize) {
                focuspoint.resetMobileTouchLinks();
            }
        },

        isOutOfDocument: function (point, posLeft, posTop, popoverWidth, popoverHeight) {
            var mainFrameOffset = $(".focuspoint").offset();
            var pointOffset = $(point).offset();
            var windowWidth = $(window).width();
            var windowHeight = $(window).height();

            var popoverLeftBorder = pointOffset.left - (popoverWidth / 2);
            var overflowLeft = mainFrameOffset.left > popoverLeftBorder;

            var popoverRightBorder = pointOffset.left + (popoverWidth / 2);
            var overflowRight = popoverRightBorder > windowWidth;

            var popoverTopBorder = pointOffset.top - (popoverHeight / 2);
            var overflowTop = mainFrameOffset.top > popoverTopBorder;

            var popoverBottomBorder = pointOffset.top + (popoverHeight / 2);
            var overflowBottom = popoverBottomBorder > windowHeight;

            return {
                top: overflowTop,
                right: overflowRight,
                bottom: overflowBottom,
                left: overflowLeft
            }
        },

        preventMobileTouchLinks: function (){
            $(".focuspoint a").on("click", function (e) {
                e.preventDefault();
                var link = $(this)[0];
                var clicked = link.clicked;
                if (!clicked) {
                    // first time clicked
                    link.clicked = 1;
                } else {
                    // second link click: open page
                    window.location.href = $(this).attr("href");
                }
            });
        },

        resetMobileTouchLinks: function () {
            $(".focuspoint a").each(function (){
                $(this)[0].clicked = null;
            });
        }
    };

    focuspoint.init();
})(jQuery);