<?php

namespace Blueways\BwFocuspointImages\ViewHelpers;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;

class MathViewHelper extends AbstractTagBasedViewHelper
{
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('number1', '', 'number', true, '');
        $this->registerArgument('number2', '', 'number', true, '');
        $this->registerArgument('operator', 'string', 'operator', true, '+');
    }

    /**
     *
     */
    public function render() : string
    {
        $number1 = $this->arguments['number1'];
        $number2 = $this->arguments['number2'];
        $operator = $this->arguments['operator'];

        return eval("return $number1 $operator $number2;");
    }
}
